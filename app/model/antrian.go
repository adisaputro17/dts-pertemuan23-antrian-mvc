package model

import (
	"firebase.google.com/go/db"
	"fmt"
	"log"
	"strings"
	"time"
)

func AddAntrian() (bool, error) {
	_, _, dataAntrian := GetAntrian()
	var Id string
	var antrianRef *db.Ref
	ref := client.NewRef("antrian")

	if dataAntrian == nil {
		Id = fmt.Sprintf("A-0")
		antrianRef = ref.Child("0")
	} else {
		Id = fmt.Sprintf("A-%d", len(dataAntrian))
		antrianRef = ref.Child(fmt.Sprintf("%d", len(dataAntrian)))
	}

	if err := antrianRef.Set(ctx, map[string]interface{}{
		"id":     Id,
		"status": false,
		"starting at":	time.Now().String(),
	}); err != nil {
		log.Fatal(err)
		return false, err
	}
	return true, nil
}

func GetAntrian() (bool, error, []map[string]interface{}) {
	var data []map[string]interface{}
	ref := client.NewRef("antrian")
	if err := ref.Get(ctx, &data); err != nil {
		log.Fatalln("Error reading from database:", err)
		return false, err, nil
	}
	return true, nil, data
}

func UpdateAntrian(idAntrian string) (bool, error) {
	ref := client.NewRef("antrian")
	id := strings.Split(idAntrian, "-")
	childRef := ref.Child(id[1])
	
	if err := childRef.Update(ctx, map[string]interface{}{
		"id":     idAntrian,
		"status": true,
		"finished at":	time.Now().String(),
	}); err != nil {
		log.Fatal(err)
		return false, err
	}
	return true, nil
}

func DeleteAntrian(idAntrian string) (bool, error) {
	ref := client.NewRef("antrian")
	id := strings.Split(idAntrian, "-")
	childRef := ref.Child(id[1])
	if err := childRef.Delete(ctx); err != nil {
		log.Fatal(err)
		return false, err
	}
	return true, nil
}